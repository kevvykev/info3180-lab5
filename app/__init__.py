
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
import os
from flask.ext.login import LoginManager
from flask.ext.openid import OpenID


app = Flask(__name__)
app.config['SECRET_KEY'] = "this is a super secure key"
# remember to change to heroku's database
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://action@localhost/action"
db = SQLAlchemy(app)
lm = LoginManager()
lm.init_app(app)
oid = OpenID(app, '/tmp')

from app import views, models
